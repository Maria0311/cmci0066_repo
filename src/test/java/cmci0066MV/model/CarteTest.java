package cmci0066MV.model;

import org.junit.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c, c1;

    @Before
    public void setUp() throws Exception {
        c=new Carte();
        c.setTitlu("Povesti");
        c.setEditura("Litera");

        List<String> lista = new ArrayList<>();
        lista.addAll(Arrays.asList("girafa", "leu", "zebra"));
        c.setCuvinteCheie(lista);

        System.out.println("Before test");
    }

    @After
    public void tearDown() throws Exception {
        c=null;
        System.out.println("After test");
    }

    @Ignore("getTitlu is not tested, it is in progress")
    @Test
    public void getTitlu() {
        assertEquals("Povesti", c.getTitlu());
    }

    @Test
    public void setTitlu() {
        c.setTitlu("Povestiri");
        assertEquals("Povestiri", c.getTitlu());
    }

    @BeforeClass
    public static void Setup(){
        System.out.println("Before any test");
    }

    @AfterClass
    public static void Setdown(){
        System.out.println("After all test");
    }

    @Test(timeout = 10)
    public void getEditura(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
           // e.printStackTrace();
        }

        assertEquals("Litera", c.getEditura());
    }

    @Ignore
    @Test(expected = NullPointerException.class)
    public void testConstructor(){
        assertEquals(false, c1);
    }

    @Test(expected = Exception.class)
    public void setAnAparitie() throws Exception {
        c.setAnAparitie("1800");
    }


    /* Tema */

    @Test
    public void setAutori(){
       List<String> arList = new ArrayList<>();
       arList.addAll(Arrays.asList("Ion Creanga", "Mihail Sadoveanu"));
       c.setAutori(arList);

       List<String> arListExpected = new ArrayList<>(arList);
       assertEquals(arListExpected, c.getAutori());

    }

    @Test
    public void setCuvinteCheie() {
        List<String> arList = new ArrayList<>();
        arList.addAll(Arrays.asList("lup", "urs", "caprioara"));
        c.setCuvinteCheie(arList);

        List<String> arListExpected = new ArrayList<>(arList);
        assertEquals(arListExpected, c.getCuvinteCheie());
    }

    @Test
    public void deleteCuvantCheie() {
        List<String> arListExpected = new ArrayList<>();
        arListExpected.addAll(Arrays.asList("girafa", "zebra"));

        c.deleteCuvantCheie("leu");
        assertEquals(arListExpected, c.getCuvinteCheie());

    }

    @Test(timeout = 10)
    public void setEditura() {
        try {
            Thread.sleep(100);
            c.setEditura("Teora");
        } catch (InterruptedException e) {
            // e.printStackTrace();
        }

        assertEquals("Teora", c.getEditura());
    }

}
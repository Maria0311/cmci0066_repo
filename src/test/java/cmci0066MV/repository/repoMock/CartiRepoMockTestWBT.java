package cmci0066MV.repository.repoMock;

import cmci0066MV.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTestWBT {
    private CartiRepoMock repo;
    private Carte c1, c2;
    private String autoriA[];
    private ArrayList<String> autori;
    private String autor = "a1";

    @Before
    public void setUp() throws Exception {
        repo = new CartiRepoMock();

        c1 = new Carte();
        c1.setTitlu("c1");
        autoriA=new String[]{"a2"};
        autori= new ArrayList<String>(Arrays.asList(autoriA));
        c1.setAutori(autori);

        c2 = new Carte();
        c2.setTitlu("c2");
        autoriA=new String[]{"a1"};
        autori= new ArrayList<String>(Arrays.asList(autoriA));
        c2.setAutori(autori);
    }

    @After
    public void tearDown() throws Exception {
        repo = null;
        c1 = null;
        c2 = null;
        autoriA = null;
        autori = null;
    }

    @Test
    public void cautaCarte1() {
        List<Carte> carti = repo.cautaCarte(autor);
        assertEquals(carti.size(), 0);
    }

    @Test
    public void cautaCarte2() {
        repo.adaugaCarte(c1);
        repo.adaugaCarte(c2);

        List<Carte> carti = repo.cautaCarte(autor);
        assertEquals(carti.size(), 1);
    }
}
package cmci0066MV.repository.repoInterfaces;

import cmci0066MV.model.Carte;
import java.util.List;

public interface CartiRepoInterface {
	void adaugaCarte(Carte c);
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte c);
	List<Carte> cautaCarte(String autor);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(String an);
}
